#include <iostream>
#include <fstream>
#include <unistd.h>

using namespace std;

int main()
{
    ifstream inFile("/sys/class/thermal/thermal_zone3/temp");
    while(true) {
        string therm;
    
        if (inFile.fail()){
            cerr <<"Wrong file"<< endl;
            return 1;
        }  

        while (inFile >> therm) {
            system("clear");
            int temp = stoi(therm) / 1000;
            cout << "Temperature: " << temp << endl;
            inFile.seekg(0, inFile.beg);
            sleep(1);
            if(temp >= 40 && temp < 50) {
                system("echo e > /dev/ttyUSB0");
            } else if(temp >= 50 && temp < 60) {
                system("echo w > /dev/ttyUSB0");
            } else if(temp >= 60) {
                system("echo q > /dev/ttyUSB0");
            }
        }
        
    }

    inFile.close();

    return 0;
}