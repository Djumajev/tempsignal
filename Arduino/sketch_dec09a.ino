const byte dynPin = 2;

float notes[] = {
  493.88,
  440,
  392.00,
  349.23,
  329.63,
  293.66,
  261.63
  };

void setup() {
    Serial.begin(9600);
    pinMode( dynPin, OUTPUT ); // Настраиваем контакт на выход 
}

void loop() {
  char a = Serial.read();
  switch(a){
    case 'q':
      tone(dynPin, notes[0]);
      delay(100);
      noTone(dynPin);
      break;
    case 'w':
      tone(dynPin, notes[1]);
      delay(100);
      noTone(dynPin);
      break;
    case 'e':
      tone(dynPin, notes[2]);
      delay(100);
      noTone(dynPin);
      break;
    case 'r':
      tone(dynPin, notes[3]);
      delay(100);
      noTone(dynPin);
      break;
   case 't':
      tone(dynPin, notes[4]);
      delay(100);
      noTone(dynPin);
      break;
   case 'y':
      tone(dynPin, notes[5]);
      delay(100);
      noTone(dynPin);
      break;
   case 'u':
      tone(dynPin, notes[6]);
      delay(100);
      noTone(dynPin);
      break;
   case '1':
      for(int i = 0; i < 7; i++){
        notes[i] *= 2;
      }
      break;
  case '2':
      for(int i = 0; i < 7; i++){
        notes[i] /= 2;
      }
      break;
  }
}
